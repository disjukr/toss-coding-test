# toss-coding-test
웹 브라우저판 토스 클론입니다.

[데모 페이지](https://disjukr.gitlab.io/toss-coding-test/)

## 지원 브라우저
맥OS와 안드로이드의 크롬 62, 파이어폭스 57 에서 정상동작 확인하였습니다.

iOS는 고려하지 않았습니다.

## 미리보기
![preview](preview.gif)

## 설치 및 돌려보기
### 요구사항
- nodejs 9.0.0 이상

```sh
npm install
npm run start
```
