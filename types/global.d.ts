type Falsy = false | 0 | '' | null | undefined;
type Nullable<T> = T | null | undefined;
