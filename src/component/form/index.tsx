import * as React from 'react';
import * as classNames from 'classnames';
import Slider from 'react-slick';
import { sprintf } from 'sprintf-js';

import Button from '../Button';
import Screen from '../Screen';
import transfer, { Account } from '../../fixture/transfer';
import * as styles from './styles.css';


interface LabelProps {
    hide?: boolean;
}

export const Label: React.SFC<LabelProps> = ({
    children,
    hide,
}) => (
    <div className={classNames(styles['label'], {
        [styles['hide']]: hide,
    })}>
        { children }
    </div>
);


interface InputProps {
    handleInput?: () => void;
    label: string;
    showLabel?: boolean;
    placeholder?: string;
    className?: string;
    valueClassName?: string;
}

export const Input: React.SFC<InputProps> = ({
    children,
    handleInput,
    label,
    placeholder,
    className,
    valueClassName,
}) => {
    const childrenArray = React.Children.toArray(children);
    const showPlaceholder = childrenArray.filter(child => child).length === 0;
    return (
        <div
            className={classNames(
                styles['input'],
                { [styles['click']]: !!handleInput },
                className,
            )}
            onClick={handleInput}>
            <Label hide={showPlaceholder}>{ label }</Label>
            <div className={classNames(styles['value'], valueClassName)}>
                { showPlaceholder ? <div className={styles['placeholder']}>
                    { placeholder }
                </div> : children }
            </div>
        </div>
    );
};

interface InputScreenProps {
    title: string;
    cancel: () => void;
    onConfirmButtonClick: () => void;
    onSubmit?: () => void;
    className?: string;
    contentClassName?: string;
}

export const InputScreen: React.SFC<InputScreenProps> = ({
    title,
    cancel,
    children,
    onConfirmButtonClick,
    onSubmit,
    className,
    contentClassName,
}) => (
    <Screen
        title={title}
        noTitleShadow={true}
        hasBackButton={true}
        close={cancel}
        className={className}
        contentClassName={classNames(
            styles['input-screen-content'],
            contentClassName,
        )}>
        <form
            className={styles['content']}
            onSubmit={e => {
                onSubmit && onSubmit();
                e.preventDefault();
            }}>
            { children }
        </form>
        <Button
            label="확인"
            type="primary"
            noRadius={true}
            onClick={onConfirmButtonClick}
        />
    </Screen>
);


interface AccountInputProps {
    label: string;
    value: string;
    amount: number;
    onChange?: (value: string) => void;
    className?: string;
}

export class AccountInput extends React.Component<AccountInputProps> {
    static validate(amount: number, account: Account) {
        const fee = account.fee.free.remain < 1 ? account.fee.amount : 0;
        return (amount + fee) <= account.deposit.amount;
    }
    render() {
        const {
            label,
            value,
            amount,
            onChange,
            className,
        } = this.props;
        const account = this.getAccount(value)!;
        const index = transfer.mine.accounts.indexOf(account);
        return (
            <div
                className={classNames(
                    styles['input'],
                    styles['account-input'],
                    className,
                )}>
                <Label>{ label }</Label>
                <AccountInputSlider
                    accounts={transfer.mine.accounts}
                    initialSlide={index}
                    onChange={index => {
                        if (onChange) onChange(this.getIndexValue(index));
                    }}
                />
                <AccountInputDots
                    index={index}
                    count={transfer.mine.accounts.length}
                />
                <AccountInputValidationNote
                    amount={amount}
                    account={account}
                    isValid={AccountInput.validate(amount, account)}
                />
            </div>
        );
    }
    private getAccount(value: string) {
        return transfer.mine.accounts.find(
            account => value === account.account
        );
    }
    private getIndexValue(index: number) {
        return transfer.mine.accounts[index].account;
    }
}

interface AccountInputSliderProps {
    accounts: Account[];
    initialSlide: number;
    onChange: (value: number) => void;
}

class AccountInputSlider extends React.Component<AccountInputSliderProps> {
    private slider: Slider | null;
    private static formatDeposit(deposit: { amount: number, currency: string }) {
        const currency = {
            KRW: '원',
        }[deposit.currency];
        return `${ deposit.amount.toLocaleString() }${ currency }`;
    }
    private static accountDetail(value: Account) {
        const {
            account,
            corporation,
            deposit,
        } = value;
        return account ?
            `${ corporation.name } ${ account.replace(/^(....)..../, '$1****') }` :
            AccountInputSlider.formatDeposit(deposit);
    }
    // react-slick 터치 슬라이드 중 재렌더되면 애니메이션이 작동 안함
    // 무조건 재렌더가 필요한 속성이 아닌 이상 재렌더 무시하도록 만듬
    shouldComponentUpdate(nextProps: AccountInputSliderProps) {
        const { accounts } = this.props;
        if (accounts !== nextProps.accounts) return true;
        return false;
    }
    render() {
        const {
            accounts,
            initialSlide,
            onChange,
        } = this.props;
        return (
            <div className={styles['slider-container']}>
                <Slider
                    className={styles['slider']}
                    ref={ref => this.slider = ref}
                    initialSlide={initialSlide}
                    beforeChange={(oldIndex, index) => {
                        if (oldIndex === index) return;
                        onChange && onChange(index);
                    }}
                    speed={100}
                    arrows={false}
                    infinite={false}>
                    { accounts.map(account => <div key={account.account}>
                        <div className={styles['account']}>
                            <div className={styles['name']}>
                                { account.name }
                            </div>
                            <div className={styles['detail']}>
                                { AccountInputSlider.accountDetail(account) }
                            </div>
                        </div>
                    </div>) }
                </Slider>
            </div>
        );
    }
}

interface AccountInputDotsProps {
    index: number;
    count: number;
}

const AccountInputDots: React.SFC<AccountInputDotsProps> = ({
    index,
    count,
}) => {
    const dotWidth = 8 + 4; // width: 8, padding sum: 4
    const totalWidth = dotWidth * count;
    const halfWidth = totalWidth / 2;
    const offset = dotWidth * index;
    return (
        <div className={styles['account-input-dots']}>
            { Array(count).fill(1).map(
                (_, i) => <div key={i} className={styles['dot']}/>
            ) }
            <div
                style={{ left: `calc(50% + ${ offset - halfWidth }px)` }}
                className={classNames(styles['dot'], styles['curr'])}
            />
        </div>
    );
};

interface AccountInputValidationNoteProps {
    amount: number;
    account: Account;
    isValid: boolean;
}

const AccountInputValidationNote: React.SFC<AccountInputValidationNoteProps> = ({
    amount,
    account,
    isValid,
}) => (
    <div className={styles['account-input-validation-note']}>
        <div className={styles['text']}>
            { sprintf(
                account.validate[`${ isValid }`],
                account.fee.free.remain,
                account.fee.free.monthly,
            ) }
        </div>
    </div>
);
