import * as React from 'react';
import * as classNames from 'classnames';

import * as styles from './styles.css';


export type ButtonType = 'default' | 'primary';

interface ButtonProps {
    label: string;
    disabled?: boolean;
    type?: ButtonType;
    noRadius?: boolean;
    className?: string;
    onClick?: () => void;
}

interface ButtonState {
    ripples: (RippleProps & { key: number })[];
}

export default class Button extends React.Component<ButtonProps, ButtonState> {
    private div: HTMLDivElement | null;
    private rippleKey = 0;
    private static isTouchEvent(event: any): event is React.TouchEvent<{}> {
        const { changedTouches } = event as React.TouchEvent<{}>;
        return changedTouches && 'length' in changedTouches;
    }
    componentWillMount() {
        this.setState({
            ripples: [],
        });
    }
    render() {
        const {
            label,
            disabled,
            type,
            noRadius,
            className,
            onClick,
        } = this.props;
        const {
            ripples,
        } = this.state;
        const onPress = disabled ? undefined : this.onPress;
        const isTouchDevice = 'ontouchstart' in document;
        return (
            <div
                className={classNames(
                    styles['button'],
                    {
                        [styles['no-radius']]: noRadius,
                        [styles['disabled']]: disabled,
                        [styles['primary']]: type === 'primary',
                    },
                    className,
                )}
                ref={ref => this.div = ref}
                onMouseDown={isTouchDevice ? undefined : onPress}
                onTouchStart={isTouchDevice ? onPress : undefined}
                onClick={disabled ? undefined : onClick}>
                { ripples.map(ripple => <Ripple key={ripple.key} {...ripple}/>) }
                <div className={styles['label']}>
                    { label }
                </div>
            </div>
        );
    }
    private addRipple(ripple: RippleProps) {
        const {
            ripples,
        } = this.state;
        const key = this.rippleKey++;
        ripple.onTransitionEnd = () => this.setState({
            ripples: this.state.ripples.filter(ripple => ripple.key !== key),
        });
        this.setState({
            ripples: ripples.concat({ ...ripple, key }),
        });
    }
    private onPress = (e: React.TouchEvent<{}> | React.MouseEvent<{}>) => {
        if (this.div) {
            const { top, left, width, height } = this.div.getBoundingClientRect();
            const beginSize = Math.min(width, height) * 0.5;
            const endSize = Math.min(width, height) * 4;
            let pageX: number = 0, pageY: number = 0;
            if (Button.isTouchEvent(e)) {
                ({ pageX, pageY } = e.changedTouches[0]);
            } else {
                ({ pageX, pageY } = e);
            }
            this.addRipple({
                top: pageY - top,
                left: pageX - left,
                beginSize,
                endSize,
            });
        }
    }
}

interface RippleProps {
    top: number;
    left: number;
    beginSize: number;
    endSize: number;
    onTransitionEnd?: () => void;
}

interface RippleState {
    transitioning: boolean;
}

export class Ripple extends React.Component<RippleProps, RippleState> {
    private div: HTMLDivElement | null;
    componentWillMount() {
        this.setState({
            transitioning: false,
        });
    }
    componentDidMount() {
        if (this.div) this.div.offsetHeight;
        this.setState({
            transitioning: true,
        });
    }
    render() {
        const {
            top,
            left,
            beginSize,
            endSize,
            onTransitionEnd,
        } = this.props;
        const { transitioning } = this.state;
        const size = transitioning ? endSize : beginSize;
        return (
            <div
                ref={ref => this.div = ref}
                className={styles['ripple']}
                style={{
                    top,
                    left,
                    width: size,
                    height: size,
                    opacity: transitioning ? 0 : 0.6,
                }}
                onTransitionEnd={onTransitionEnd}
            />
        );
    }
}
