import * as React from 'react';

import Button from '../Button';
import TossLogo from '../TossLogo';
import Layer from '../Layer';
import RemitScreen from '../RemitScreen';
import * as styles from './styles.css';


interface AppState {
    isRemitScreenOpen: boolean;
}

class App extends React.Component<{}, AppState> {
    componentWillMount() {
        this.setState({
            isRemitScreenOpen: false,
        });
    }
    render() {
        const {
            isRemitScreenOpen,
        } = this.state;
        return (
            <div className={styles['app']}>
                <TossLogo white={true}/>
                <Button
                    label="송금할 금액 입력"
                    onClick={() => this.setState({ isRemitScreenOpen: true })}
                    className={styles['remit-button']}
                />
                <Layer content={isRemitScreenOpen && <RemitScreen
                    close={() => this.setState({ isRemitScreenOpen: false })}
                />}/>
            </div>
        );
    }
}

export default App;
