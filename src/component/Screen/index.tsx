import * as React from 'react';
import * as classNames from 'classnames';

import * as styles from './styles.css';


interface RemitScreenProps {
    className?: string;
    contentClassName?: string;
    title: string;
    noTitleShadow?: boolean;
    hasBackButton?: boolean;
    close?: () => void;
}

export default class Screen extends React.Component<RemitScreenProps> {
    componentDidMount() {
        // input의 autoFocus 속성으로 인한 브라우저 스크롤 동작 우회용
        document.getElementById('root')!.scrollLeft = 0;
    }
    render() {
        const {
            children,
            className,
            contentClassName,
            title,
            noTitleShadow,
            hasBackButton,
            close,
        } = this.props;
        return (
            <div className={classNames(styles['screen'], className)}>
                <div className={classNames(styles['title'], {
                    [styles['no-title-shadow']]: noTitleShadow,
                })}>
                    { hasBackButton && <div
                        className={styles['back-button']}
                        onClick={close}>
                        { '\u2190' }
                    </div> }
                    { title }
                </div>
                <div className={classNames(styles['content'], contentClassName)}>
                    { children }
                </div>
            </div>
        );
    }
}
