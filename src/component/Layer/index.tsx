import * as React from 'react';
import * as classNames from 'classnames';

import * as styles from './styles.css';


interface LayerProps {
    content: React.ReactChild | Falsy;
}

interface LayerState {
    content: React.ReactChild | null;
    height: string | null;
}

export default class Layer extends React.Component<LayerProps, LayerState> {
    private div: HTMLDivElement | null;
    componentWillMount() {
        this.setState({
            content: this.props.content || null,
            height: null,
        });
    }
    componentDidMount() {
        // 2012/11/18 기준 (chrome 62, firefox 57)
        // 모바일 크롬에서 상단 주소창이 하단의 확인버튼을 가리는 문제 우회.
        // 모바일 파이어폭스의 경우 아래의 우회책을 적용하면 키보드가 올라왔을 때
        // 확인버튼이 같이 올라오는 순기능을 이용할 수 없음
        if ((window as any).chrome) {
            this.setState({ height: `${ window.innerHeight }px` });
        }
    }
    componentWillReceiveProps(nextProps: LayerProps) {
        if (!this.state.content) {
            this.setState({ content: nextProps.content || null });
        }
    }
    onTransitionEnd = (e: React.TransitionEvent<HTMLDivElement>) => {
        if (e.target !== this.div) return;
        this.setState({ content: this.props.content || null });
    }
    render() {
        const {
            content,
            height,
        } = this.state;
        return (
            <div
                className={classNames(styles['layer'], {
                    [styles['open']]: !!this.props.content,
                })}
                style={height !== null ? { height } : undefined}
                ref={ref => this.div = ref}
                onTransitionEnd={this.onTransitionEnd}>
                { content && <div className={styles['content']}>
                    { content }
                </div> }
            </div>
        );
    }
}
