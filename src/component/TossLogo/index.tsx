import * as React from 'react';
import * as classNames from 'classnames';

import * as styles from './styles.css';


interface TossLogoProps {
    white?: boolean;
}

const TossLogo: React.SFC<TossLogoProps> = ({ white }) => (
    <img
        className={classNames({ [styles['white']]: white })}
        src={require('./logo.svg')}
    />
);

export default TossLogo;
