import * as React from 'react';
import * as classNames from 'classnames';

import { remit } from '../../api';
import transfer, { Account } from '../../fixture/transfer';
import {
    AccountInput,
    Input,
    InputScreen,
} from '../form';
import Button from '../Button';
import Layer from '../Layer';
import Screen from '../Screen';
import * as styles from './styles.css';


interface RemitScreenProps {
    close?: () => void;
}

interface RemitScreenState {
    amount: number;
    isAmountInputScreenOpen: boolean;
    receiver: string;
    message: string;
    isMessageInputScreenOpen: boolean;
    accountId: string;
}

export default class RemitScreen extends React.Component<RemitScreenProps, RemitScreenState> {
    static validate(
        amount: number,
        receiver: string,
        message: string,
        account: Account,
    ) {
        if (amount <= 0) return false;
        if (!AccountInput.validate(amount, account)) return false;
        if (message.length > transfer.message.limit) return false;
        return true;
    }
    componentWillMount() {
        this.setState({
            amount: transfer.amount.current,
            isAmountInputScreenOpen: true,
            receiver: '아무개',
            message: '',
            isMessageInputScreenOpen: false,
            accountId: '', // empty string is default toss account
        });
    }
    render() {
        const {
            close,
        } = this.props;
        const {
            amount,
            isAmountInputScreenOpen,
            receiver,
            message,
            isMessageInputScreenOpen,
            accountId,
        } = this.state;
        const account = transfer.mine.accounts.find(
            item => accountId === item.account
        )!;
        return (
            <Screen
                title="송금하기"
                hasBackButton={true}
                close={close}>
                <Input
                    className={styles['amount-input']}
                    handleInput={() => this.setState({ isAmountInputScreenOpen: true })}
                    label="보낼 금액"
                    placeholder="보낼 금액을 입력하세요.">
                    { (amount > 0) && amount.toLocaleString() }
                    { (amount > 0) && <div className={styles['currency']}>원</div> }
                </Input>
                <Input
                    className={styles['receiver-input']}
                    handleInput={() => {/* TODO: input screen */}}
                    label="받는 분"
                    placeholder="받는 분 (계좌번호 또는 연락처)">
                    { receiver }
                    <div className={styles['info']}>
                        (받는 분 입력 미구현)
                    </div>
                </Input>
                <Input
                    className={styles['message-input']}
                    valueClassName={styles['message-input-value']}
                    handleInput={() => this.setState({ isMessageInputScreenOpen: true })}
                    label="메시지 (선택사항)">
                    { message ? <div>
                        { message }
                    </div> : <div className={styles['placeholder']}>
                        문자로 보낼 메시지 입력
                    </div> }
                    <div className={styles['count']}>
                        { message.length } / { transfer.message.limit }
                    </div>
                </Input>
                <AccountInput
                    label="출금 계좌"
                    value={accountId}
                    amount={amount}
                    onChange={accountId => this.setState({ accountId })}
                />
                <div className={styles['padding']}/>
                <Button
                    label="보내기"
                    type="primary"
                    className={styles['confirm-button']}
                    disabled={!RemitScreen.validate(
                        amount,
                        receiver,
                        message,
                        account,
                    )}
                    onClick={async () => {
                        await remit(
                            `${ amount }`,
                            'johndoe',
                            message,
                            accountId,
                        );
                    }}
                />
                <Layer content={isAmountInputScreenOpen && <AmountInputScreen
                    close={() => this.setState({ isAmountInputScreenOpen: false })}
                    value={amount}
                    confirm={amount => this.setState({ amount })}
                />}/>
                <Layer content={isMessageInputScreenOpen && <MessageInputScreen
                    close={() => this.setState({ isMessageInputScreenOpen: false })}
                    value={message}
                    confirm={message => this.setState({ message })}
                    limit={transfer.message.limit}
                />}/>
            </Screen>
        );
    }
}

interface CommonInputScreenProps<TValue> {
    close: () => void;
    value: TValue;
    confirm: (value: TValue) => void;
}

interface AmountInputScreenProps extends CommonInputScreenProps<number> {}

interface AmountInputScreenState {
    value: number;
    error: boolean;
}

class AmountInputScreen extends React.Component<AmountInputScreenProps, AmountInputScreenState> {
    private input: HTMLInputElement | null;
    private shakeKey = 0;
    componentWillMount() {
        this.setState({
            value: this.props.value,
            error: false,
        });
    }
    confirmAndClose = () => {
        const {
            close,
            confirm,
        } = this.props;
        const { value } = this.state;
        confirm(value);
        close();
    }
    render() {
        const {
            close,
        } = this.props;
        const {
            value,
            error,
        } = this.state;
        const { daily: dailyLimit, remain } = transfer.amount.limit;
        return (
            <InputScreen
                className={styles['amount-input-screen']}
                title="금액 입력"
                cancel={close}
                onConfirmButtonClick={this.confirmAndClose}
                onSubmit={this.confirmAndClose}>
                <div
                    className={styles['input']}
                    onClick={() => {
                        if (!this.input) return;
                        const { value } = this.input;
                        this.input.focus();
                        this.input.setSelectionRange(value.length, value.length);
                    }}>
                    <input
                        className={styles['real-input']}
                        ref={ref => this.input = ref}
                        // tslint:disable-next-line:max-line-length
                        // https://stackoverflow.com/questions/21177489/selectionstart-selectionend-on-input-type-number-no-longer-allowed-in-chrome
                        // 의미상 type="number"를 써야하지만 (그래야 올바른 키패드가 올라오지만)
                        // 크롬에서는 number type input에서는 setSelectionRange 사용이 불가능합니다.
                        // 그래서 setSelectionRange 사용이 가능하면서 비슷한 키패드가 올라오는 type="tel"을 썼습니다.
                        type="tel"
                        value={value}
                        onChange={e => {
                            const str = e.target.value.replace(/[^0-9]/g, '');
                            const value = Math.max(+str | 0, 0);
                            const error = value > remain;
                            if (error && navigator.vibrate) {
                                 navigator.vibrate(500);
                            }
                            this.setState({
                                value: Math.min(value, remain),
                                error,
                            });
                        }}
                        autoFocus={true}
                    />
                    <div key={this.shakeKey++} ref={error ? ref => {
                        if (ref) ref.style.animation = 'shake 400ms';
                    } : undefined}>
                        { value.toLocaleString() } 원
                    </div>
                </div>
                { error ? <div className={classNames(styles['info'], styles['error'])}>
                    1일 { dailyLimit.toLocaleString() }원
                    까지만 이체할 수 있습니다.
                </div> : <div className={styles['info']}>
                    최대 { remain.toLocaleString() }원
                </div> }
            </InputScreen>
        );
    }
}

interface MessageInputScreenProps extends CommonInputScreenProps<string> {
    limit: number;
}

interface MessageInputScreenState {
    value: string;
}

class MessageInputScreen extends React.Component<MessageInputScreenProps, MessageInputScreenState> {
    componentWillMount() {
        this.setState({
            value: this.props.value,
        });
    }
    confirmAndClose = () => {
        const {
            close,
            confirm,
        } = this.props;
        const { value } = this.state;
        confirm(value);
        close();
    }
    render() {
        const {
            close,
            limit,
        } = this.props;
        const {
            value,
        } = this.state;
        return (
            <InputScreen
                className={styles['message-input-screen']}
                title="함께 보낼 메시지"
                cancel={close}
                onConfirmButtonClick={this.confirmAndClose}
                onSubmit={this.confirmAndClose}>
                <input
                    className={styles['input']}
                    type="text"
                    placeholder="문자로 보낼 메시지 입력"
                    value={value}
                    onChange={e => this.setState({ value: e.target.value })}
                    maxLength={limit}
                    autoFocus={true}
                />
                <div className={styles['count']}>
                    { value.length } / { limit }
                </div>
            </InputScreen>
        );
    }
}
