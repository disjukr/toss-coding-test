import * as data from './transfer.json';


export interface Account {
    name: string;
    corporation: {
        id: string,
        name: string,
    };
    account: string;
    deposit: {
        amount: number,
        currency: string,
    };
    validate: { [key: string]: string }; // key: 'true' | 'false'
    fee: {
        amount: number,
        currency: string,
        free: {
            monthly: number,
            remain: number,
        },
    };
}

export interface Transfer {
    amount: {
        current: number,
        limit: {
            daily: number,
            remain: number,
        },
    };
    message: {
        limit: number,
    };
    mine: {
        id: string,
        name: string,
        contact: {
            cellphone: string,
        },
        accounts: Account[],
    };
}

const transferData: Transfer = data;

export default transferData;
