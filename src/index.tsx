import * as React from 'react';
import * as ReactDOM from 'react-dom';

import App from './component/App';
import './styles.css';


ReactDOM.render(
    <App />,
    document.getElementById('root') as HTMLElement
);
