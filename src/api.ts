export async function remit(
    amount: string,
    receiverId: string,
    message: string,
    accountId: string,
) {
    const payload = {
        amount,
        receiverId,
        message,
        accountId,
    };
    console.info(
        [
            '%c서버로 전송할 Payload:',
            '',
            '%cPOST /api/remit%c',
            'Content-Type: application/json; charset=utf-8',
            JSON.stringify(payload, null, 4),
        ].join('\n'),
        'font-size: 20px; font-weight: bold;',
        'font-size: 18px; font-weight: bold;',
        'font-size: 16px; font-weight: normal;',
    );
}
